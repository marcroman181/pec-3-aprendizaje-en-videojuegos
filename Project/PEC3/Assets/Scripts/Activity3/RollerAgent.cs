﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class RollerAgent : Agent
{
    Rigidbody rBody;
    public Transform Target;
    public float forceMultiplier = 10;
    public Transform Traps;
    public Transform Zones;
    private bool episodeEnded;
    private Transform trainingArea;
    private RandomizeLabirinth RandomizeLabirinth;

    void Start()
    {
        rBody = GetComponent<Rigidbody>();
        trainingArea = transform.parent.transform;
        RandomizeLabirinth = new RandomizeLabirinth(Traps, Target, Zones);
    }
    public override void OnEpisodeBegin()
    {
        // If the Agent fell, zero its momentum
        if (this.transform.localPosition.y < 0)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(0, 0.5f, 0);
        }

        // Move the target to a new spot
        RandomizeLabirinth.InitializeLabirinth();
        SpawnAgent();

        episodeEnded = false;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Target.localPosition);
        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = actionBuffers.ContinuousActions[0];
        controlSignal.z = actionBuffers.ContinuousActions[1];
        rBody.AddForce(controlSignal * forceMultiplier);

        // Fell off platform
        if (this.transform.localPosition.y < 0)
        {
            SetReward(-1.0f);
            episodeEnded = true;
            EndEpisode();
        }
        AddReward(-1f / 50000);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[0] = Input.GetAxis("Horizontal");
        continuousActionsOut[1] = Input.GetAxis("Vertical");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Target") && !episodeEnded)
        {
            // Reached target
            SetReward(1.0f);
            episodeEnded = true;
            EndEpisode();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Target") && !episodeEnded)
        {
            // Reached target
            SetReward(1.0f);
            episodeEnded = true;
            EndEpisode();
        }
    }
    private void SpawnAgent()
    {


        //Activity 3 parte 1
        //this.transform.localPosition = new Vector3(0, 0.5f, 0);


        //Activity 3 parte 2
        this.rBody.angularVelocity = Vector3.zero;
        this.rBody.velocity = Vector3.zero;
        
        this.transform.localPosition = new Vector3(ObtainSpawnPoint(), 0.5f, ObtainSpawnPoint());
    }

    private int ObtainSpawnPoint()
    {
        int pisition = Random.Range(0, 4);
        switch (pisition)
        {
            case 0:
                return -4;
                break;
            case 1:
                return -1;
                break;
            case 2:
                return 1;
                break;
            case 3:
                return 4;
                break;
        }
        return 1;
    }

}

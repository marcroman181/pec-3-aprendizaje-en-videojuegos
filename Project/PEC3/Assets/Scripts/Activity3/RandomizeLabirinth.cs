﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeLabirinth
{
    private List<GameObject> Traps;
    private Transform Target;
    private List<GameObject> Zones;

    public RandomizeLabirinth(Transform TrapsContainer, Transform target, Transform zones)
    {
        Target = target;
        Traps = new List<GameObject>();
        foreach (Transform child in TrapsContainer)
        {
            Traps.Add(child.gameObject);
        }
        Zones = new List<GameObject>();
        foreach (Transform child in zones)
        {
            Zones.Add(child.gameObject);
        }
    } 

    public void InitializeLabirinth()
    {
        SpawnTarget();
        EnableTraps();
        //InitializeZones();
    }

    private void SpawnTarget()
    {
        int wall = Random.Range(0, 4);
        int position = Random.Range(0, 4);
        float x = 4.75f;
        float z = 4.75f;
        float positionFloat = 0;

        switch (position)
        {
            case 0:
                positionFloat = -3.75f;
                break;
            case 1:
                positionFloat = -1.25f;
                break;
            case 2:
                positionFloat = 1.25f;
                break;
            case 3:
                positionFloat = 3.75f;
                break;
        }

        switch (wall)
        {
            case 0:
                x = positionFloat;
                break;
            case 1:
                z = positionFloat;
                break;
            case 2:
                x = positionFloat;
                z = -z;
                break;
            case 3:
                z = positionFloat;
                x = -x;
                break;
        }

        Target.localPosition = new Vector3(x, 1.1f, z);
    }

    private void EnableTraps()
    {
        for (int i = 0; i < Traps.Count; i++)
        {
            Traps[i].SetActive(true);
        }

        int numberTraps = Random.Range(3, 6);


        for (int i = 0; i < numberTraps; i++)
        {
            int indexTrap = Random.Range(0, Traps.Count);
            Traps[indexTrap].SetActive(false);
        }
    }

    private void InitializeZones()
    {
        for(int i = 0; i < Zones.Count; i++)
        {
            Zones[i].GetComponent<Zone>().InitializeZone();
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
    public List<GameObject> ConfigurationA;
    public List<GameObject> ConfigurationB;
    public List<GameObject> ConfigurationC;
    private List<GameObject> Walls;

    void Start()
    {

        Walls = new List<GameObject>();
        foreach (Transform child in transform)
        {
            Walls.Add(child.gameObject);
        }
    }

    public void InitializeZone()
    {
        ResetZone();
        int configuration = Random.Range(0, 3);
        switch (configuration)
        {
            case 0:
                InitializeConfiguration(ConfigurationA);
                break;
            case 1:
                InitializeConfiguration(ConfigurationB);
                break;
            case 2:
                InitializeConfiguration(ConfigurationC);
                break;
        }
    }

    public void ResetZone()
    {
        for (int i = 0; i < Walls.Count; i++)
        {
            Walls[i].SetActive(false);
        }
    }

    public void InitializeConfiguration(List<GameObject> Configuration)
    {

        for (int i = 0; i < Configuration.Count; i++)
        {
            Configuration[i].SetActive(true);
        }
    }
}
